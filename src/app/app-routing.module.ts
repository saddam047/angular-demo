import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminforgotpasswordComponent } from './frontcomponents/adminforgotpassword/adminforgotpassword.component';
import { AdminresetpasswordComponent } from './frontcomponents/adminresetpassword/adminresetpassword.component';
import { HomepageComponent } from './frontcomponents/homepage/homepage.component';
import { LoginAdminComponent } from './frontcomponents/login-admin/login-admin.component';
import { DefaultComponent } from './layouts/default/default.component';
import { FrontedComponent } from './layouts/fronted/fronted.component';
import { TeacherLayoutComponent } from './layouts/teacher-layout/teacher-layout.component';
import { AdminauthGuard } from './modules/guard/adminauth.guard';
import { TeacherauthGuard } from './teacher/teacherauth.guard';

const routes: Routes = [

  {
    path: "",
    pathMatch: "full",
    redirectTo:"/teacher",
  },
  {
    path:"adminLogin",
    component:LoginAdminComponent
  },
  {
   path:"adminforgotpassword",
   component:AdminforgotpasswordComponent
  },
  {
    path:"adminresetpassword/:token",
    component:AdminresetpasswordComponent
   },
  {
    path:"admin",
    component:DefaultComponent,
    canActivate:[AdminauthGuard],
    loadChildren: ()=>import("./modules/admin/admin.module").then(m=>m.AdminModule)
  },
  {
    path:"teacher",
    component:TeacherLayoutComponent,
    loadChildren:()=>import("./teacher/teacher/teacher.module").then(m=>m.TeacherModule)
  },
  {
    path:"teacherdash",
    component:TeacherLayoutComponent,
    canActivate:[TeacherauthGuard],
    loadChildren:()=>import("./teacher/teacherdash/teacherdash.module").then(m=>m.TeacherdashModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
