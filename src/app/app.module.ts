import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DefaultModule } from './layouts/default/default.module';
import { LoginAdminComponent } from './frontcomponents/login-admin/login-admin.component';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { FrontedModule } from './layouts/fronted/fronted.module';

import { TeacherLayoutModule } from './layouts/teacher-layout/teacher-layout.module';
import { AdminforgotpasswordComponent } from './frontcomponents/adminforgotpassword/adminforgotpassword.component';
import { AdminresetpasswordComponent } from './frontcomponents/adminresetpassword/adminresetpassword.component';
import { MyaccountComponent } from './modules/myaccount/myaccount.component';
import { ThankyouComponent } from './teacher/thankyou/thankyou.component';
import { SignupStep1Component } from './teacher/signup-step1/signup-step1.component';
import { TeacherloginComponent } from './teacher/teacherlogin/teacherlogin.component';
import { ThomeComponent } from './teacher/thome/thome.component';
import { ThomebannerComponent } from './teacher/thomebanner/thomebanner.component';





@NgModule({
  declarations: [
    AppComponent,

    LoginAdminComponent,

    
    AdminforgotpasswordComponent,

    AdminresetpasswordComponent,

    MyaccountComponent,

    ThankyouComponent,

    TeacherloginComponent,

    SignupStep1Component,

    ThomeComponent,

    ThomebannerComponent,

 
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DefaultModule,
    FrontedModule,
    TeacherLayoutModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
