import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-teacherheader',
  templateUrl: './teacherheader.component.html',
  styleUrls: ['./teacherheader.component.css']
})
export class TeacherheaderComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }

}
