import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { RouterModule } from '@angular/router';
import { FrontedheaderComponent } from './components/frontedheader/frontedheader.component';
import { FrontedfooterComponent } from './components/frontedfooter/frontedfooter.component';
import { TeacherfooterComponent } from './components/teacherfooter/teacherfooter.component';
import { TeacherheaderComponent } from './components/teacherheader/teacherheader.component';
import { TeachersidebarComponent } from './components/teachersidebar/teachersidebar.component';



@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    FrontedheaderComponent,
    FrontedfooterComponent,
    TeacherfooterComponent,
    TeacherheaderComponent,
    TeachersidebarComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    FrontedheaderComponent,
    FrontedfooterComponent,
    TeacherheaderComponent,
    TeachersidebarComponent,
    TeacherfooterComponent
  ]
})
export class SharedModule { }
