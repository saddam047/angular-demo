import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private ROOT_URL=environment.Api_Url+"/api/auth";
  
  constructor(private http:HttpClient,private router:Router) { }
  
  adminLogin(admin){
    //console.log(admin);
    return this.http.post<any>(this.ROOT_URL+"/adminlogin",admin);
  }

  adminforgotpassword(admin){
    return this.http.post<any>(this.ROOT_URL+"/adminforgotpassword",admin);
  }

  adminresetpassword(admin){
    return this.http.post<any>(this.ROOT_URL+"/adminresetpassword",admin);
  }

  adminNewPassword(admin){
    return this.http.post<any>(this.ROOT_URL+"/adminNewPassword",admin);
  }

  adminLogOut(){
    localStorage.removeItem("Admintoken");
    this.router.navigate(["/"]);
  }
  adminloggedIn(){
      return !!localStorage.getItem("Admintoken");
  }

}
