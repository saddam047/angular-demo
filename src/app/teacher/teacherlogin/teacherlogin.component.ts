import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/shared/service/user.service';

@Component({
  selector: 'app-teacherlogin',
  templateUrl: './teacherlogin.component.html',
  styleUrls: ['./teacherlogin.component.css']
})
export class TeacherloginComponent implements OnInit {

  teacherlogfrom= new FormGroup({
    email: new FormControl("",
    [Validators.required,
    Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')
    ]),
     password:new FormControl("",Validators.required) 
  });

  constructor(private service:UserService,private router:Router) { }

  ngOnInit(): void {
  }

  teacherlogin(){
    this.router.navigate(['/admin']);
  }

}
