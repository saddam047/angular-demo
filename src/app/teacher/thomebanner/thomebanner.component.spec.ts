import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThomebannerComponent } from './thomebanner.component';

describe('ThomebannerComponent', () => {
  let component: ThomebannerComponent;
  let fixture: ComponentFixture<ThomebannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThomebannerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThomebannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
