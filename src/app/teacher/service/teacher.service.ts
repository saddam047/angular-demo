import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {
  private ROOT_URL=environment.Api_Url+"/api/auth";
  constructor(private http:HttpClient,private router:Router) { }

  teacherLogin(teacher){  
    //console.log(teacher);
    return this.http.post<any>(this.ROOT_URL+"/teacherlogin",teacher);
  }

  teachersignup(teacher){  
    //console.log(teacher);
    return this.http.post<any>(this.ROOT_URL+"/teachersignup",teacher);
  }



  teacherLogOut(){
    localStorage.removeItem("teachertoken");
    this.router.navigate(["/"]);
  }
  teacherloggedIn(){
      return !!localStorage.getItem("teachertoken");
  }
}
