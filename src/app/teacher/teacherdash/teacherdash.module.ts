import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeacherdashRoutingModule } from './teacherdash-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TeacherdashRoutingModule
  ]
})
export class TeacherdashModule { }
