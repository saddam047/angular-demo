import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { TeacherService } from './service/teacher.service';

@Injectable({
  providedIn: 'root'
})
export class TeacherauthGuard implements CanActivate {
  constructor(private teacherservice:TeacherService,private router:Router){}
  canActivate():boolean {
    if (this.teacherservice.teacherloggedIn()) {
      return true;
    } else {
      this.router.navigate(['teacher/teacherlog']);
      return false;
    }
  }
  
}
