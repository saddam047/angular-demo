import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { TeacherService } from '../service/teacher.service';

@Component({
  selector: 'app-signup-step1',
  templateUrl: './signup-step1.component.html',
  styleUrls: ['./signup-step1.component.css']
})
export class SignupStep1Component implements OnInit {
  submitted = false;
  teachersignupfrom=new FormGroup({
    name:new FormControl("",Validators.required),
    email: new FormControl("",
    [Validators.required,
    Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')
    ]),
    password:new FormControl("",Validators.required),
    passwordConfirm:new FormControl("",Validators.required),
    gender:new FormControl("",Validators.required),
    phone:new FormControl("",Validators.required),
    qualification:new FormControl(""),
    specialization:new FormControl("",Validators.required),
    experience:new FormControl("",Validators.required),
  },{validators: this.checkPasswords });
  constructor(private router:Router,private teacherservice:TeacherService) { }

  ngOnInit(): void {
  }
  get f() { return this.teachersignupfrom.controls; }
  teachersignup(){
     if(this.teachersignupfrom.valid){
        this.teacherservice.teachersignup(this.teachersignupfrom.value).subscribe(res=>{
          console.log(res);
          localStorage.setItem("teachertoken",res.token);
          this.teachersignupfrom.reset();
          this.router.navigate(['/teacher/teacherlog']);
        },
        err=>{
          console.log(err);
        });
    }
  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
    let pass = group.get('password').value;
    let confirmPass = group.get('passwordConfirm').value;

    return pass === confirmPass ? null : { notSame: true }     
  }

}
