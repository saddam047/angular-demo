import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignupStep1Component } from '../signup-step1/signup-step1.component';
import { TeacherloginComponent } from '../teacherlogin/teacherlogin.component';
import { ThomeComponent } from '../thome/thome.component';

const routes: Routes = [
  // {
  //   path: "teacher",
  //   pathMatch: "full",
  //   redirectTo:"/teacherlog",
  // },
  {
    path:"",
    component:TeacherloginComponent
  },
  {
    path:"signup-step1",
    component:SignupStep1Component
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeacherRoutingModule { }
