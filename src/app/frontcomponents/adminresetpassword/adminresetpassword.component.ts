import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/shared/service/user.service';

@Component({
  selector: 'app-adminresetpassword',
  templateUrl: './adminresetpassword.component.html',
  styleUrls: ['./adminresetpassword.component.css']
})
export class AdminresetpasswordComponent implements OnInit {
   Token:string;
  adminresetfrom= new FormGroup({
    token: new FormControl("", Validators.required),
    password: new FormControl("", Validators.required),
    confirm_password: new FormControl("",Validators.required)
  });

  constructor(private userservice:UserService,private router:Router,private activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params=>{
      const token=params['token'];
      this.Token=token;
      console.log(token);
    });
  }
  adminresetpassword(){
    if(this.adminresetfrom.valid){
      this.userservice.adminresetpassword(this.adminresetfrom.value).subscribe(res=>{
        console.log(res);
        if(res.status){
           this.userservice.adminNewPassword(this.adminresetfrom.value).subscribe(res1=>{
                console.log(res1);
                this.router.navigate(['/admin']);
                this.adminresetfrom.reset();
           },
          err1=>{
            console.log(err1);
          });
        }
        
        //this.router.navigate(['/admin']);
      },
      err=>{
        console.log(err);
      });
    }
  }
}
