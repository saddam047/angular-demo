import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/shared/service/user.service';

@Component({
  selector: 'app-adminforgotpassword',
  templateUrl: './adminforgotpassword.component.html',
  styleUrls: ['./adminforgotpassword.component.css']
})
export class AdminforgotpasswordComponent implements OnInit {
   
  adminforgotform= new FormGroup({
    email: new FormControl("",Validators.required)
  });

  constructor(private userservice:UserService, private router:Router) { }

  ngOnInit(): void {
  }

  adminforgot(){
    if(this.adminforgotform.valid){
      this.userservice.adminforgotpassword(this.adminforgotform.value).subscribe(res=>{
        console.log(res);
        this.adminforgotform.reset();
        //this.router.navigate(['/admin']);
      },
      err=>{
        console.log(err);
      });
    }
  }

}
