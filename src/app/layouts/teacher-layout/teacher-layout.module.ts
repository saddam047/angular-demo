import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeacherLayoutComponent } from './teacher-layout.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({
  declarations: [
    TeacherLayoutComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule
  ]
})
export class TeacherLayoutModule { }
