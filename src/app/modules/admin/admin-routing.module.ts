import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../home/home.component';
import { MyaccountComponent } from '../myaccount/myaccount.component';

const routes: Routes = [
  {
    path:"",
    component:HomeComponent
  },
  {
     path:"myaccount",
     component:MyaccountComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
