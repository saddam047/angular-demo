import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.component.html',
  styleUrls: ['./myaccount.component.css']
})
export class MyaccountComponent implements OnInit {
   
  adminaccountform=new FormGroup({
    email:new FormControl("",Validators.required),
    image:new FormControl("",Validators.required),
    name:new FormControl("",Validators.required),
  });
  constructor(private router:Router) { }

  ngOnInit(): void {
  }

}
